Pod::Spec.new do |s|

s.platform = :ios
s.ios.deployment_target = '11.0'
s.name = "GISPod"
s.summary = "GISPod is the pod to be used as a core/engine for the GISProject"

s.version = "0.1.83"


s.license = { :type => "MIT", :file => "LICENSE" }


s.author = { "Leon Berni" => "leon.berni@valid.com" }


s.homepage = "https://bitbucket.org/validcertificadora/gis-ios"

s.source = { :git => "https://lbernivalid@bitbucket.org/lbernivalid/gisframework-ios.git",
             :tag => "#{s.version}" }
             
s.public_header_files = "GISPod.framework/Headers/*.h"
s.source_files = "GISPod.framework/Headers/*.h"
s.vendored_frameworks = "GISPod.framework"

s.swift_version = "5.0"

end
