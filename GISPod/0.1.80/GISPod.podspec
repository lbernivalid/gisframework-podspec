Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '11.0'
s.name = "GISPod"
s.summary = "GISPod is the pod to be used as a core/engine for the GISProject"
s.requires_arc = true

# 2
s.version = "0.1.80"
s.license = { :type => "MIT", :file => "LICENSE" }
s.author = { "Leon Berni" => "leon.berni@valid.com" }
s.homepage = "https://bitbucket.org/validcertificadora/gis-ios"


s.ios.vendored_frameworks = 'GISPod.framework'
s.source = { :http => "https://bitbucket.org/lbernivalid/gisframework-ios/raw/0f4d39f3bba8d2bcc79c3b81ff5a3022e9a17951/GISPod.zip"}
s.exclude_files = "Classes/Exclude"
s.swift_version = "5.0"

end
